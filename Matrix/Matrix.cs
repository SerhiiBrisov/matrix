using System;
using System.Collections.Generic;
using System.Runtime.Serialization;
using System.Security.Cryptography;
using System.Security.Permissions;
using System.Text;

namespace MatrixLibrary
{
    [Serializable]
    public class MatrixException : Exception
    {
        private readonly string resourceName;
        private readonly IList<string> validationErrors;
        public MatrixException() { }
        public MatrixException(string message)
            : base(message)
        {
        }

        public MatrixException(string message, Exception innerException)
            : base(message, innerException)
        {
        }

        public MatrixException(string message, string resourceName, IList<string> validationErrors)
            : base(message)
        {
            this.resourceName = resourceName;
            this.validationErrors = validationErrors;
        }

        public MatrixException(string message, string resourceName, IList<string> validationErrors, Exception innerException)
            : base(message, innerException)
        {
            this.resourceName = resourceName;
            this.validationErrors = validationErrors;
        }

        [SecurityPermissionAttribute(SecurityAction.Demand, SerializationFormatter = true)]
        // Constructor should be protected for unsealed classes, private for sealed classes.
        // (The Serializer invokes this constructor through reflection, so it can be private)
        protected MatrixException(SerializationInfo info, StreamingContext context)
            : base(info, context)
        {
            this.resourceName = info.GetString("ResourceName");
            this.validationErrors = (IList<string>)info.GetValue("ValidationErrors", typeof(IList<string>));
        }

        public string ResourceName
        {
            get { return this.resourceName; }
        }

        public IList<string> ValidationErrors
        {
            get { return this.validationErrors; }
        }

        [SecurityPermissionAttribute(SecurityAction.Demand, SerializationFormatter = true)]
        public override void GetObjectData(SerializationInfo info, StreamingContext context)
        {
            if (info == null)
            {
                throw new ArgumentNullException("info");
            }

            info.AddValue("ResourceName", this.ResourceName);

            // Note: if "List<T>" isn't serializable you may need to work out another
            //       method of adding your list, this is just for show...
            info.AddValue("ValidationErrors", this.ValidationErrors, typeof(IList<string>));

            // MUST call through to the base class to let it save its own state
            base.GetObjectData(info, context);
        }
    }



    public class Matrix : ICloneable
    {
        private readonly double[,] matrix;
        private readonly int rows;
        private readonly int columns;

        /// <summary>
        /// Number of rows.
        /// </summary>
        public int Rows
        {
            get => rows;
        }

        /// <summary>
        /// Number of columns.
        /// </summary>
        public int Columns
        {
            get => columns;
        }

        /// <summary>
        /// Gets an array of floating-point values that represents the elements of this Matrix.
        /// </summary>
        public double[,] Array
        {
            get => matrix;
        }

        /// <summary>
        /// Initializes a new instance of the <see cref="Matrix"/> class.
        /// </summary>
        /// <param name="rows"></param>
        /// <param name="columns"></param>
        /// <exception cref="ArgumentOutOfRangeException"></exception>
        public Matrix(int rows, int columns)
        {
            if (rows < 0)
            {
                throw new ArgumentOutOfRangeException("rows");
            }
            if (columns < 0)
            {
                throw new ArgumentOutOfRangeException("columns");
            }
            matrix = new double[rows, columns];
            this.rows = rows;
            this.columns = columns;
        }

        /// <summary>
        /// Initializes a new instance of the <see cref="Matrix"/> class with the specified elements.
        /// </summary>
        /// <param name="array">An array of floating-point values that represents the elements of this Matrix.</param>
        /// <exception cref="ArgumentNullException"></exception>
        public Matrix(double[,] array)
        {
            if (array is null)
            {
                throw new ArgumentNullException("array");
            }
            matrix = array;
            rows = array.GetUpperBound(0) + 1;
            columns = array.GetUpperBound(array.Rank - 1) + 1;

        }

        /// <summary>
        /// Allows instances of a Matrix to be indexed just like arrays.
        /// </summary>
        /// <param name="row"></param>
        /// <param name="column"></param>
        /// <exception cref="ArgumentException"></exception>
        public double this[int row, int column]
        {
            get
            {
                if (row < 0 || row > rows)
                {
                    throw new ArgumentException("row");
                }
                if (column < 0 || column > columns)
                {
                    throw new ArgumentException("column");
                }
                return matrix[row, column];
            }
            set
            {
                if (row < 0 || row > rows)
                {
                    throw new ArgumentException("row");
                }
                if (column < 0 || column > columns)
                {
                    throw new ArgumentException("column");
                }
                matrix[row, column] = value;
            }
        }

        /// <summary>
        /// Adds two matrices.
        /// </summary>
        /// <param name="matrix1"></param>
        /// <param name="matrix2"></param>
        /// <returns>New <see cref="Matrix"/> object which is sum of two matrices.</returns>
        /// <exception cref="ArgumentNullException"></exception>
        /// <exception cref="MatrixException"></exception>
        public static Matrix operator +(Matrix matrix1, Matrix matrix2)
        {
            if (matrix1 is null)
            {

                throw new ArgumentNullException("matrix1");
            }
            if (matrix2 is null)
            {

                throw new ArgumentNullException("matrix2");
            }
            if (matrix1.Rows != matrix2.Rows! || matrix1.Columns != matrix2.Columns!)
            {
                throw new MatrixException();
            }
            Matrix result = matrix1.Clone() as Matrix;
            for (int i = 0; i < matrix2.Rows; i++)
            {
                for (int j = 0; j < matrix2.Columns; j++)
                {
                    result[i, j] += matrix2[i, j];
                }
            }
            return result;
        }

        /// <summary>
        /// Subtracts two matrices.
        /// </summary>
        /// <param name="matrix1"></param>
        /// <param name="matrix2"></param>
        /// <returns>New <see cref="Matrix"/> object which is subtraction of two matrices</returns>
        /// <exception cref="ArgumentNullException"></exception>
        /// <exception cref="MatrixException"></exception>
        public static Matrix operator -(Matrix matrix1, Matrix matrix2)
        {
            if (matrix1 is null)
            {

                throw new ArgumentNullException("matrix1");
            }
            if (matrix2 is null)
            {

                throw new ArgumentNullException("matrix2");
            }
            if (matrix1.Rows != matrix2.Rows! || matrix1.Columns != matrix2.Columns!)
            {
                throw new MatrixException();
            }
            Matrix result = matrix1.Clone() as Matrix;
            for (int i = 0; i < matrix2.Rows; i++)
            {
                for (int j = 0; j < matrix2.Columns; j++)
                {
                    result[i, j] -= matrix2[i, j];
                }
            }
            return result;
        }

        /// <summary>
        /// Multiplies two matrices.
        /// </summary>
        /// <param name="matrix1"></param>
        /// <param name="matrix2"></param>
        /// <returns>New <see cref="Matrix"/> object which is multiplication of two matrices.</returns>
        /// <exception cref="ArgumentNullException"></exception>
        /// <exception cref="MatrixException"></exception>
        public static Matrix operator *(Matrix matrix1, Matrix matrix2)
        {
            if (matrix1 is null)
            {

                throw new ArgumentNullException("matrix1");
            }
            if (matrix2 is null)
            {

                throw new ArgumentNullException("matrix2");
            }
            if (matrix1.Rows != matrix2.Columns! && matrix2.Rows != matrix1.Columns!)
            {
                throw new MatrixException("Multiplication unvailible! Number of columns of one matrix not equal number of rows of another matrix");
            }
            Matrix result = new Matrix(matrix1.Rows, matrix2.Columns);

            for (var i = 0; i < matrix1.Rows; i++)
            {
                for (var j = 0; j < matrix2.Columns; j++)
                {
                    result[i, j] = 0;

                    for (var k = 0; k < matrix1.Columns; k++)
                    {
                        result[i, j] += matrix1[i, k] * matrix2[k, j];
                    }
                }
            }
            return result;
        }

        /// <summary>
        /// Adds <see cref="Matrix"/> to the current matrix.
        /// </summary>
        /// <param name="matrix"><see cref="Matrix"/> for adding.</param>
        /// <exception cref="ArgumentNullException"></exception>
        /// <exception cref="MatrixException"></exception>
        public Matrix Add(Matrix matrix)
        {
            return this + matrix;
        }

        /// <summary>
        /// Subtracts <see cref="Matrix"/> from the current matrix.
        /// </summary>
        /// <param name="matrix"><see cref="Matrix"/> for subtracting.</param>
        /// <exception cref="ArgumentNullException"></exception>
        /// <exception cref="MatrixException"></exception>
        public Matrix Subtract(Matrix matrix)
        {
            return this - matrix;
        }

        /// <summary>
        /// Multiplies <see cref="Matrix"/> on the current matrix.
        /// </summary>
        /// <param name="matrix"><see cref="Matrix"/> for multiplying.</param>
        /// <exception cref="ArgumentNullException"></exception>
        /// <exception cref="MatrixException"></exception>
        public Matrix Multiply(Matrix matrix)
        {
            return this * matrix;
        }

        /// <summary>
        /// Tests if <see cref="Matrix"/> is identical to this Matrix.
        /// </summary>
        /// <param name="obj">Object to compare with. (Can be null)</param>
        /// <returns>True if matrices are equal, false if are not equal.</returns>
        /// <exception cref="InvalidCastException">Thrown when object has wrong type.</exception>
        /// <exception cref="MatrixException">Thrown when matrices are incomparable.</exception>
        public override bool Equals(object obj)
        {
            if (!(obj is Matrix))
            {
                return false;
            }
            var matrix2 = obj as Matrix;
            if (matrix2.rows != rows || matrix2.columns != columns)
            {
                return false;
            }
            bool flag = true;
            for (int i = 0; i < rows; i++)
            {
                for (int j = 0; j < columns; j++)
                {
                    if (matrix[i, j] != (obj as Matrix).matrix[i, j])
                    {
                        flag = false;
                    }
                }
            }
            return flag;
        }

        /// <summary>
        /// Creates a deep copy of this Matrix.
        /// </summary>
        /// <returns>A deep copy of the current object.</returns>
        public object Clone()
        {
            double[,] newMatrix = new double[Rows, Columns];
            for (int i = 0; i < Rows; i++)
            {
                for (int j = 0; j < Columns; j++)
                {
                    newMatrix[i, j] = matrix[i, j];
                }
            }
            return ((new Matrix(newMatrix)) as object);
        }


        public override int GetHashCode()
        {
            var mD5Crypto = new MD5CryptoServiceProvider();
            StringBuilder hashCode = new StringBuilder();
            foreach (var item in matrix)
            {
                hashCode.Insert(hashCode.Length, mD5Crypto.ComputeHash(UTF8Encoding.Default.GetBytes(item.ToString())).ToString());
            }
            return HashCode.Combine(Int32.Parse(hashCode.ToString().ToLower().Replace("-", "")), "myCode");
        }
    }
}
